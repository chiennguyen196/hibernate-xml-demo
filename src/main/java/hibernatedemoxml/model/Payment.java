package hibernatedemoxml.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Chien Nguyen on 21/06/2017.
 */
public class Payment {
    private PaymentPK paymentPK;
    private Customer customer;
    private String checkNumber;
    private Date paymentDate;
    private Double amount;

    public Payment() {
    }

    public Payment(Customer customer, String checkNumber) {
        this.customer = customer;
        this.checkNumber = checkNumber;
        this.paymentPK = new PaymentPK(customer.getCustomerNumber(), checkNumber);
    }

//        public Payment(Customer customer, String checkNumber) {
//        this.paymentPK = new PaymentPK(customer, checkNumber);
//    }

    public PaymentPK getPaymentPK() {
        return paymentPK;
    }

    public void setPaymentPK(PaymentPK paymentPK) {
        this.paymentPK = paymentPK;
    }


    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }


//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Payment)) return false;
//
//        Payment payment = (Payment) o;
//
//        return getPaymentPK() != null ? getPaymentPK().equals(payment.getPaymentPK()) : payment.getPaymentPK() == null;
//    }
//
//    @Override
//    public int hashCode() {
//        return getPaymentPK() != null ? getPaymentPK().hashCode() : 0;
//    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Payment)) return false;

        Payment payment = (Payment) o;

        return getPaymentPK() != null ? getPaymentPK().equals(payment.getPaymentPK()) : payment.getPaymentPK() == null;
    }

    @Override
    public int hashCode() {
        return getPaymentPK() != null ? getPaymentPK().hashCode() : 0;
    }
}

