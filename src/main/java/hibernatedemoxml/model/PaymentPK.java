package hibernatedemoxml.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Chien Nguyen on 22/06/2017.
 */
public class PaymentPK implements Serializable {
   private int customerNumber;
   private String checkNumber;
   public PaymentPK() {
   }

    public PaymentPK(int customerNumber, String checkNumber) {
        this.customerNumber = customerNumber;
        this.checkNumber = checkNumber;
    }

    public int getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PaymentPK)) return false;

        PaymentPK paymentPK = (PaymentPK) o;

        if (getCustomerNumber() != paymentPK.getCustomerNumber()) return false;
        return getCheckNumber() != null ? getCheckNumber().equals(paymentPK.getCheckNumber()) : paymentPK.getCheckNumber() == null;
    }

    @Override
    public int hashCode() {
        int result = getCustomerNumber();
        result = 31 * result + (getCheckNumber() != null ? getCheckNumber().hashCode() : 0);
        return result;
    }
}
