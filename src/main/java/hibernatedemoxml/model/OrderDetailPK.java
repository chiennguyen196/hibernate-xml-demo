package hibernatedemoxml.model;

import java.io.Serializable;

/**
 * Created by Chien Nguyen on 22/06/2017.
 */
public class OrderDetailPK implements Serializable {
    private Integer orderNumber;
    private String productCode;

    public OrderDetailPK() {
    }

    public OrderDetailPK(Integer orderNumber, String productCode) {
        this.orderNumber = orderNumber;
        this.productCode = productCode;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderDetailPK)) return false;

        OrderDetailPK that = (OrderDetailPK) o;

        if (getOrderNumber() != null ? !getOrderNumber().equals(that.getOrderNumber()) : that.getOrderNumber() != null)
            return false;
        return getProductCode() != null ? getProductCode().equals(that.getProductCode()) : that.getProductCode() == null;
    }

    @Override
    public int hashCode() {
        int result = getOrderNumber() != null ? getOrderNumber().hashCode() : 0;
        result = 31 * result + (getProductCode() != null ? getProductCode().hashCode() : 0);
        return result;
    }
}
