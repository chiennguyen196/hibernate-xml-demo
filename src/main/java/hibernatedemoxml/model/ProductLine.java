package hibernatedemoxml.model;

import java.io.Serializable;

/**
 * Created by Chien Nguyen on 21/06/2017.
 */
public class ProductLine {
    private String productLine;
    private String textDescription;
    private String htmlDescription;
    private Serializable image;

    public ProductLine() {
    }

    public ProductLine(String productLine) {
        this.productLine = productLine;
    }

    public String getProductLine() {
        return productLine;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public String getTextDescription() {
        return textDescription;
    }

    public void setTextDescription(String textDescription) {
        this.textDescription = textDescription;
    }

    public String getHtmlDescription() {
        return htmlDescription;
    }

    public void setHtmlDescription(String htmlDescription) {
        this.htmlDescription = htmlDescription;
    }

    public Serializable getImage() {
        return image;
    }

    public void setImage(Serializable image) {
        this.image = image;
    }
}
